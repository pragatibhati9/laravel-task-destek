@extends('layout') 

@section('content') 
<div class="card p-4 mt-4">
	<h4>Register User</h4>
	<form action="{{ route('add-user')}}" method="post">
		@csrf

					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		<div class="row">
			  <div class="col-6 form-group mb-3">
			    <label for="name">Name: </label>
			    <input type="name"  name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name">
			  </div>

			  <div class="col-6 form-group mb-3">
			    <label for="email">Email: </label>
			    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
			  </div>

			  <div class="col-6 form-group mb-3">
			    <label for="mobile">Mobile: </label>
			    <input type="mobile" name="mobile" class="form-control" id="mobile" placeholder="Enter mobile">
			  </div>

			  <div class="col-6 form-group mb-3">
			    <label for="referal_by">Referal Code: </label>
			    <input type="referal_by" name="referal_by" class="form-control" id="referal_by" placeholder="Enter Referal Code">
			  </div>
			  <div class="col-6 form-group mb-3">
			    <label>Gender</label>
				  <div class="form-check">
				    <label for="male">Male</label>
				    <input type="radio" class="form-check-input" name="gender" value="male" id="male">
				  </div>
				  <div class="form-check">
				    <label for="female">Female</label>
				    <input type="radio" class="form-check-input" name="gender" value="female" id="female">
				  </div>
			  </div>
			  <div class="col-6 form-group mb-3">
			      <label>Technology</label>
			  	  <div class="form-check">
			  	    <label for="php">Php</label>
			  	    <input type="checkbox" class="form-check-input" name="technology[]" value="php" id="php">
			  	  </div>
			  	  <div class="form-check">
			  	    <label for="angular">Angular</label>
			  	    <input type="checkbox" class="form-check-input" name="technology[]" value="angular" id="angular">
			  	  </div>
			  	  <div class="form-check">
			  	    <label for="nodejs">Nodejs</label>
			  	    <input type="checkbox" class="form-check-input" name="technology[]" value="nodejs" id="nodejs">
			  	  </div>
			 </div>
		</div>
	  <button type="reset" class="btn btn-danger">Cancel</button>
	  <button type="submit" class="btn btn-primary">Submit</button>
	   @if(session('success'))
						    <div class="alert alert-success mt-4">
						        {{ session('success') }}
						    </div>
						 @endif
	</form>
</div>
@endsection
