@extends('layout') 

@section('content') 
<div class="row">
	<div class="col-12">
		<table class="table border mt-4">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Referal code</th>
					<th>Gender</th>
					<th>Technolgy</th>
					<th>Points</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if($user->count())
				@foreach ($user as  $value)
				<tr>
					<td>{{ $value->name }}</td>
					<td>{{ $value->email }}</td>
					<td>{{ $value->mobile }}</td>
					<td>{{ $value->referal_code }}</td>
					<td>{{ $value->gender }}</td>
					<td>{{ $value->technology }}</td>
					<td>{{ $value->points }}</td>
					<td><a href='{{ route("referal-users",["id"=>$value->referal_code]) }}' class="btn btn-outline-primary">Referal List</a></td>
				</tr>
				@endforeach
				@else
				<tr>
					<td colspan="8">No user</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@endsection
