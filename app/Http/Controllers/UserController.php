<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
class UserController extends Controller
{
    public function index(){
        return view('register');
    }

    public function addUser(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'unique:users,email',
            'mobile' => 'required|max:10|min:10',
            'gender' => 'required',
            'technology.*' => 'required',
        ]);

        if(!empty($request->referal_by)){
          $exist = User::where('referal_code',$request->referal_by)->exists();
          if($exist == true){
             $referal_by = $request->referal_by;
          }else{
            return back()->withErrors(['msg'=>'Referal Code is not valid!!']);
          }
        }
        // dd($request->technology);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->technology = implode(",",$request->technology);
        $user->gender = $request->gender;
        if(isset($referal_by)){
          $user->points = 10;
          $user->referal_by = $request->referal_by;
        }
        $user->referal_code = $this->referalcode();
        $user->save();
        if($user && isset($referal_by)){
          $user = User::where('referal_code',$request->referal_by)->first();
          $user->points = $user->points+20;
          $user->save();
        }
        return redirect()->back()->with('success', 'Form submitted successfully.');
    }

    function referalcode($length = 6) {
        do{
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[random_int(0, $charactersLength - 1)];
            }
         $randomString;
         $exist = User::where('referal_code',$randomString)->exists();
        }while($exist);
        return $randomString;
    }

    public function users(){
        $user = User::all();
        return view('users',compact('user'));
    }

    public function referalUser(Request $request, $id)
    {
        $user = User::where('referal_by',$id)->get();
        return view('users',compact('user'));
    }
}
